/*********
  Rui Santos
  Complete project details at https://RandomNerdTutorials.com
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files.
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
*********/

// Import required libraries
#ifdef ESP32
  #include <WiFi.h>
  #include <ESPAsyncWebServer.h>
  #include <SPIFFS.h>
#else
  #include <Arduino.h>
  #include <ESP8266WiFi.h>
  #include <Hash.h>
  #include <ESPAsyncTCP.h>
  #include <ESPAsyncWebServer.h>
  #include <FS.h>
#endif
#include <Wire.h>
#include "Adafruit_CCS811.h"
#include "Adafruit_Sensor.h"
#include "Adafruit_AM2320.h"
#include "time.h"

const char* ntpServer = "pool.ntp.org";
const long  gmtOffset_sec = 3600;
const int   daylightOffset_sec = 3600;


time_t now;

Adafruit_AM2320 am2320 = Adafruit_AM2320();
Adafruit_CCS811 ccs;


// Replace with your network credentials
const char* ssid = "Stockwerk";
const char* password = "stck?01!";

// Create AsyncWebServer object on port 80
AsyncWebServer server(80);

float getTemp() {
  int tries = 10;
  float t = am2320.readTemperature();
  delay(100);
  while (isnan(t) and tries-- > 0) {
    t = am2320.readTemperature();
    delay(100);
  }
  if (tries <= 1)
    return 0;
  return t;
}

float getHumid() {
  float h = am2320.readHumidity();
  delay(100);
  int tries = 10;
  while (isnan(h) and tries-- > 0) {
    h = am2320.readHumidity();
    delay(100);
  }
  if (tries <= 1)
    return 0;
  return h;
}



struct tm timeinfo;
String tmp = "";
String humidS = "";
String TVOCS = "";
String eco2S = "";

String getTempStr() { 
  return "[" + tmp + "]";
}


String getHumidStr(int len) {
  return "[" + humidS + "]";
}

String getTVOCStr() {
  return "[" + TVOCS + "]";
}

String getECO2() {
  return "[" + eco2S + "]";
}


void setup(){
  // Serial port for debugging purposes
  Serial.begin(115200);
  
  ccs.begin();
  am2320.begin();

  // Initialize SPIFFS
  if(!SPIFFS.begin()){
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }

  // Connect to Wi-Fi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi..");
  }

  // Print ESP32 Local IP Address
  Serial.println(WiFi.localIP());
  configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
  getLocalTime(&timeinfo);
  Serial.println("get time ...");
  delay(2000);
  

  //calibrate temperature sensor
  while(!ccs.available());
  float temp = ccs.calculateTemperature();
  ccs.setTempOffset(temp - getTemp());
  Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");
  // Route for root / web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/index.html");
  });
  server.on("/temperature", HTTP_GET, [](AsyncWebServerRequest *request){
    if(request->hasParam("len")) {
      String l = request->arg("len");
      Serial.print("new Len: ");
      Serial.println(l);
      /*printLen = atoi(l.c_str());*/
    }
    request->send_P(200, "text/plain", getTempStr().c_str());
  });
  server.on("/humidity", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", getHumidStr(20).c_str());
  });
  server.on("/tvoc", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", getTVOCStr().c_str());
  });
  server.on("/eco2", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", getECO2().c_str());
  });

  // Start server
  server.begin();
}

int eCO2 = 0;

int getTVOC() {
  int tvoc = 0;
  if(ccs.available()){
    //float temp = ccs.calculateTemperature();
    if(!ccs.readData()){
      tvoc = ccs.getTVOC();
      eCO2 = ccs.geteCO2();
      Serial.print("tvoc: ");
      Serial.println(tvoc);
      
      Serial.print("eco2: ");
      Serial.println(eCO2);
    }
  }
  return tvoc;
}

void loop(){

  //printCCS();
  long t = time(NULL) + 2*3600;
  tmp    += "{x:"+String(t)+"000,y:"+getTemp() + "},";
  humidS += "{x:"+String(t)+"000,y:"+getHumid() + "},";
  TVOCS += "{x:"+String(t)+"000,y:"+getTVOC() + "},";
  eco2S += "{x:"+String(t)+"000,y:"+eCO2 + "},";
  
  if (tmp.length() > 5000) { //~3d
    int ix = tmp.indexOf('}');
    tmp.remove(0,ix+2);    
    ix = humidS.indexOf('}');
    humidS.remove(0,ix+2);
  }
  
  if (tmp.length() < 1000) 
    delay(3000); //3s
  else 
    delay(30*60*1000); //30min
}
